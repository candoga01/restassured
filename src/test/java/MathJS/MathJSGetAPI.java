package MathJS;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class MathJSGetAPI {

    @Test
    public  void getMathAPI()
    {
        Response response = given().header("content-type","application/json").get("http://api.mathjs.org/v4/?expr=2*(7-3)");
        System.out.println("----> Response is = " + response.asString());
        // System.out.println(response.getStatusCode());

        Assert.assertEquals(response.getStatusCode(),200 );
        Assert.assertEquals(response.body().asString(), "8","Expected and actual results do not match");


    }

    @Test
    public void get_MathAPI()
    {
        RestAssured.baseURI = "http://api.mathjs.org/";
        Response response = given().get("/v4/?expr=2*(7-3)");
        System.out.println("----> Response is = " + response.asString());

    }


    public void get_API()
    {
        RestAssured.baseURI = "http://api.mathjs.org/";
        Response response = given().header("content-type","application/json").param("expr","2*4").get("/v4/");

        System.out.println("----> Response is = " + response.asString());

    }

    @DataProvider
    public Object[][] testdatafile()
    {
        return new Object[][]{
                {"1*2","2"},
                {"1*3","3"}

                };

    }
    @Test(dataProvider = "testdatafile")
    public void get_API4(String expr,String expected)
    {
        RestAssured.baseURI = "http://api.mathjs.org/";
        Response response = given().header("content-type","application/json").param("expr",expr).get("/v4/");

        System.out.println("----> Response is = " + response.asString());

    }




}
