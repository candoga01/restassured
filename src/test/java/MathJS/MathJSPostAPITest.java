import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class MathJSPostAPITest {

    @Test
    public void postTest()
    {
        RestAssured.baseURI = "http://api.mathjs.org/";

        // TO DO -> Using input parameters as a json file

        JSONObject requestParams = new JSONObject();
        requestParams.put("expr","a = 8 * 5");
        requestParams.put("precision","4");

        System.out.println(requestParams);
        System.out.println(requestParams.toJSONString());
        //System.out.println(requestParams.toString());




//        String reqBody = "{\n" +
//                "    \"expr\": [\n" +
//                "      \"a = 1.2 * (2 + 4.5)\",\n" +
//                "      \"a / 2\",\n" +
//                "      \"5.08 cm in inch\",\n" +
//                "      \"sin(45 deg) ^ 2\",\n" +
//                "      \"9 / 3 + 2i\",\n" +
//                "      \"b = [-1, 2; 3, 1]\",\n" +
//                "      \"det(b)\"\n" +
//                "    ],\n" +
//                "    \"precision\": 14\n" +
//                "  }";

        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/v4/");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );
        //Assert.assertEquals(postResponse.jsonPath().get("result").toString());


    }



    @Test
    public void postTest2()
    {
        RestAssured.baseURI = "http://api.mathjs.org/";

        String reqBody = "  {\n" +
                "    \"expr\": [\n" +
                "      \n" +
                "      \"5.08 cm in inch\",\n" +
                "      \"Infinity * Infinity\"\n" +
                "      \n" +
                "    ],\n" +
                "    \"precision\": 14\n" +
                "  }\n" +
                "  ";
        Response postResponse = given().header("content-type","application/json").body(reqBody).post("/v4/");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );



    }
    @Test
    public void simpleJson()
    {
        RestAssured.baseURI = "http://api.mathjs.org/";
        JSONObject requestParams = new JSONObject();
        JSONArray expArr = new JSONArray();
        expArr.add("a=1");
        expArr.add("a-1");

        requestParams.put("expr",expArr);
        requestParams.put("precision",1);

        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/v4/");
        System.out.println(postResponse.asString());

        Assert.assertEquals(postResponse.statusCode(),200);

        // Validating the response body
        System.out.println(postResponse.jsonPath());

        ArrayList exprResult = postResponse.jsonPath().get("result");
        System.out.println("---> " + exprResult.toString());

        Assert.assertEquals(exprResult.get(0).toString(),"1");
        Assert.assertEquals(exprResult.get(1).toString(),"0");




        // TO DO - Use Validation from Rest Assured.


    }




}
