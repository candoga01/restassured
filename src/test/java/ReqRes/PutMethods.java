package ReqRes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PutMethods {

    @Test
    public void updateUser()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("name", "Neha");
        requestParams.put("job", "Grad QA");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).put("/api/users/2");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );
    }
}
