package ReqRes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PatchMethods {

    @Test
    public void updateUserPatch()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("name", "Neha");
        requestParams.put("job", "Grad Consultant QA");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).patch("/api/users/2");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );
    }
}
